package p005;

/*
 * 2520 is the smallest number that can be divided by
 * each of the numbers from 1 to 10 without any remainder.
 * What is the smallest positive number that is evenly divisible by
 * all of the numbers from 1 to 20?
 */

import util.IntUtil;
import util.IterUtil;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class P005 {

  // assume factors are sorted
  static int smallestMultiple(int[] factors) {
    List<Integer> fl = Arrays.asList(IntUtil.box(factors));
    Collection<Integer> primes = IterUtil.filter(fl,
        new IterUtil.Pred<Integer>() {
          public boolean apply(Integer elt) {
            return IntUtil.isPrime(elt);
          }
        });
    int n = IntUtil.mult(IntUtil.toArray(primes));
    boolean found = false;
    while(!found && n < Integer.MAX_VALUE) {
      n++;
      found = true;
      for (int factor : factors) {
        found = found & (n % factor == 0);
      }
    }
    return n;
  }

  public static void main(String[] args) {
    int[] factors = IntUtil.range(1, 21);
    //int ans = smallestMultiple(factors);
    int ans = IntUtil.lcm(factors);
    for (int factor : factors) {
      if (ans % factor != 0) {
        System.err.printf("%d %% %d == %d\n", ans, factor, ans % factor);
      }
    }
    System.out.printf("p005: %,d\n", ans);
  }
}
