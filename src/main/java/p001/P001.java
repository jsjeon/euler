package p001;

/*
 * If we list all the natural numbers below 10 that are multiples of 3 or 5,
 * we get 3, 5, 6 and 9. The sum of these multiples is 23.
 * Find the sum of all the multiples of 3 or 5 below 1000.
 */

public class P001 {

  static boolean isMuln(int x, int n) {
    return 0 == x % n;
  }

  static boolean isMul3(int x) {
    return isMuln(x, 3);
  }

  static boolean isMul5(int x) {
    return isMuln(x, 5);
  }

  static int run() {
    int acc = 0;
    for (int i = 1; i < 1000; i++) {
      if (isMul3(i) || isMul5(i)) acc += i;
    }
    return acc;
  }

  public static void main(String[] args) {
    int ans = run();
    System.out.printf("p001: %d\n", ans);
  }

}
