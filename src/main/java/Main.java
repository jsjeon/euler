import util.StringUtil;

import java.lang.reflect.Method;
import java.io.File;
import java.io.FileFilter;
import java.net.URL;

public class Main {

  static String[] getSubs(String path) {
    File dir = new File(path);
    File[] subs = dir.listFiles(new FileFilter() {
        public boolean accept(File sub) {
          return sub.isDirectory();
        }
    });
    String [] r = new String [subs.length];
    for (int i = 0; i < subs.length; i++) {
      r[i] = subs[i].getName();
    }
    return r;
  }

  public static void main(String[] args) throws Exception {
    // build/classes/main/Main.class
    URL me = Main.class.getProtectionDomain().getCodeSource().getLocation();
    // build/classes
    String clss = new File(me.toURI()).getParent();
    // build/classes/main
    String root = new File(clss, "main").getPath();
    System.out.println(root);
    // [build/classes/main/p001, ...]
    String[] ps = getSubs(root);

    String[] targets = args.length == 0 ? ps : args;

    for (String t : targets) {
      // t = p001, cname = P001
      String cname = StringUtil.capitalize(t);
      // fullName = p001.P001
      String fullName = StringUtil.join(".", new String[] {t, cname});

      Class cls = Class.forName(fullName);
      Method mtd = cls.getDeclaredMethod("main", args.getClass());
      mtd.invoke(null, new Object[] { args });
    }
  }

}
