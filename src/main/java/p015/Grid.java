package p015;

import util.IntUtil;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;

public class Grid {

  private int _n;
  private Set<LatticePath> _paths;

  public Grid(int n) {
    this._n = n;
    this._paths = null;
  }

  public enum Direction {
    Up("/\\"), Down("\\/"), Left("<-"), Right("->");

    private final String _direction;

    private Direction(String d) {
      this._direction = d;
    }

    @Override
    public String toString() {
      return _direction;
    }
  }

  public Set<LatticePath> getLatticePaths() throws Exception {
    if (this._paths != null) return this._paths;

    this._paths = new TreeSet<LatticePath>();

    // (D, R) x ... (2 _n) times
    int n2 = 2 * this._n;
    int[] l = new int[n2];
    for (int i = 0; i < n2; i++) {
      l[i] = i < this._n ? 0 : 1;
    }

    int[][] p = IntUtil.permutate(l);
    for (int i = 0; i < p.length; i++) {
      List<Direction> path = new ArrayList<Direction>();
      for (int j = 0; j < p[i].length; j++) {
        Direction d = p[i][j] == 0 ? Direction.Down : Direction.Right;
        path.add(d);
      }
      if (LatticePath.isLattice(path)) {
        this._paths.add(new LatticePath(path));
      }
    }
    return this._paths;
  }

}
