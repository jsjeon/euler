package p015;

/*
 * Starting in the top left corner of a 2×2 grid,
 * and only being able to move to the right and down,
 * there are exactly 6 routes to the bottom right corner.
 * How many such routes are there through a 20×20 grid?
 */

import util.StringUtil;

import java.util.Set;

public class P015 {

  // NOTE: actually, it's simply 2n C n
  // i.e., among 2n steps, choose n places to move right
  // of course, remaining n steps should be to move down
  static long C(int n, int r) { // nCr
    long acc = 1;
    for (int i = 1; i <= r; i++) {
      acc = acc * (n-r+i) / i;
    }
    return acc;
  }

  public static void main(String[] args) throws Exception {
    Grid g = new Grid(2); // new Grid(5);
    Set<LatticePath> paths = g.getLatticePaths();
    System.out.println(StringUtil.join("\n", paths.toArray()));
    //System.out.printf("p015: %,d\n", paths.size());

    System.out.printf("2x2: %,d\n", C(2*2, 2));
    System.out.printf("p015: %,d\n", C(2*20, 20));
  }

}
