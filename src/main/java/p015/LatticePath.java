package p015;

import util.StringUtil;

import java.util.List;
import java.util.Iterator;

public class LatticePath implements Comparable<LatticePath> {

  // moves to either down or right same times
  // e.g., R R D D, R D R D, R D D R, D R R D, ...
  public static boolean isLattice(List<Grid.Direction> path) {
    if (path.contains(Grid.Direction.Up)) return false;
    if (path.contains(Grid.Direction.Left)) return false;

    int r_cnt = 0, d_cnt = 0;
    for (Grid.Direction d : path) {
      if (d == Grid.Direction.Down) d_cnt++;
      else r_cnt++;
    }
    return r_cnt == d_cnt;
  }

  List<Grid.Direction> _path;

  public LatticePath(List<Grid.Direction> path) throws Exception {
    if (!isLattice(path)) throw new Exception("not lattice");
    this._path = path;
  }

  @Override
  public int compareTo(LatticePath o) {
    int c = Integer.compare(this._path.size(), o._path.size());
    if (c != 0) return c;

    Iterator<Grid.Direction> i1 = this._path.iterator();
    Iterator<Grid.Direction> i2 = o._path.iterator();
    while (i1.hasNext() && i2.hasNext()) {
      Grid.Direction d1 = i1.next();
      Grid.Direction d2 = i2.next();
      if (d1 != d2) {
        return d1 == Grid.Direction.Right ? -1 : 1;
      }
    }
    return 0;
  }

  @Override
  public String toString() {
    return StringUtil.join(" ", this._path.toArray());
  }

}
