package p024;

/*
 * A permutation is an ordered arrangement of objects.
 * For example, 3124 is one possible permutation of the digits 1, 2, 3 and 4.
 * If all of the permutations are listed numerically or alphabetically,
 * we call it lexicographic order.
 * The lexicographic permutations of 0, 1 and 2 are:
 *   012   021   102   120   201   210
 * What is the millionth lexicographic permutation of
 * the digits 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?
 */

import util.IntUtil;
import util.StringUtil;

public class P024 {

  // there would be 9! = 362,880 permutation starting with 0
  // 3 * 9! > 1,000,000, thus the first digit is 2
  // 2 * 9! 'th permutation is: 2013456789
  // 1,000,000 - 2 * 9! = 274,240
  // for the next digit, as 8! = 40,320, we can spend 6 digits,
  // as 7 * 8! > 274,240 (= 1,000,000 - 2 * 9!).
  // Hence, the second digit is 0/1/3/4/5/6/7, 7.
  // 274,240 - 6 * 8! = 32,320
  // 7! = 5,040, so similarly, we can spend 6 digits.
  // That is, the third digit is 0/1/3/4/5/6/8, 8.
  // 32,320 - 6 * 7! = 2,080
  // 6! = 720, so 2 digits, i.e., fourth digit is 0/1/3, 3.

  // assume digits are sorted
  static int[] find(int nth, int[] digits) {
    int[] res = new int[digits.length];
    int rem = nth;
    int i = 0;
    while (rem > 0 && i < digits.length) {
      // find how many digits can be consumed at this point (i)
      int f = IntUtil.factorial(digits.length - 1 - i);
      int cnt = 1;
      while (f * cnt < rem) cnt++;

      int digit = 0;
      int cnt_match = 0;
      for (int j = 0; j < digits.length; j++) {
        digit = digits[j];
        // find unused cnt-th digit
        boolean used = false;
        for (int ii = 0; ii < i; ii++) {
          if (res[ii] == digit) used = true;
        }
        if (!used) cnt_match++;
        if (cnt_match == cnt) {
          //System.out.printf("res[%d] = %d\n", i, digit);
          res[i] = digit;
          break;
        }
      }

      // consume that amount and proceed to the next position
      rem -= f * (cnt - 1);
      i++;
    }
    return res;
  }

  public static void main(String[] args) {
    int[] digits = IntUtil.range(0, 10);
    int[] ans = find(1000000, digits);
    System.out.println("p024: " + StringUtil.join("", IntUtil.box(ans)));
  }

}
