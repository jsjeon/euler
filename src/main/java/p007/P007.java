package p007;

/* By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13,
 * we can see that the 6th prime is 13.
 * What is the 10 001st prime number?
 */

import util.IntUtil;

public class P007 {

  static int findPrimes(int cnt) {
    int i = 0;
    int n = 2;
    while (i < cnt) {
      if (IntUtil.isPrime(n)) {
        i++;
        // System.out.printf("%dth: %,d\n", i, n);
      }
      n++;
    }
    return n;
  }

  public static void main(String[] args) {
    // findPrimes(6);
    int ans = findPrimes(10001);
    System.out.printf("p007: %,d\n", ans);
  }
}
