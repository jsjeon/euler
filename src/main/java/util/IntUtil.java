package util;

import java.util.Collection;
import java.util.List;
import java.util.Arrays;

public class IntUtil {

  /** Type conversions */

  // int[] ~> Integer[]
  public static Integer[] box(int[] l) {
    Integer[] il = new Integer[l.length];
    for (int i = 0; i < l.length; i++) {
      il[i] = new Integer(l[i]);
    }
    return il;
  }

  // Integer[] ~> int[]
  public static int[] unbox(Integer[] l) {
    int[] ia = new int[l.length];
    for (int i = 0; i < l.length; i++) {
      ia[i] = l[i].intValue();
    }
    return ia;
  }

  // Collection<Integer> ~> int[]
  public static int[] toArray(Collection<Integer> l) {
    return unbox(l.toArray(new Integer[0]));
  }


  // except for 2 and 3, primes are 6k +/- 1
  public static boolean isPrime(long n) {
    if (n <= 3) {
      return n > 1;
    }
    if (n % 2 == 0 || n % 3 == 0) {
      return false;
    }
    for (int i = 5; i < Math.sqrt(n) + 1; i += 6) {
      if (n % i == 0 || n % (i+2) == 0) {
        return false;
      }
    }
    return true;
  }

  public static int gcd(int a, int b) {
    if (b == 0) return a;
    return gcd(b, a % b);
  }

  public static int gcd(int... varg) {
    if (varg.length <= 0) return 0;
    if (varg.length == 1) return varg[0];
    return IterUtil.fold_left(Arrays.asList(box(varg)), new Integer(varg[0]),
        new IterUtil.Func<Integer, Integer>() {
          public Integer accept(Integer acc, Integer elt) {
              return gcd(acc, elt);
          }
        });
  }

  public static int lcm(int a, int b) {
    return (a / gcd(a, b)) * b; // a * b may exceed the boundary
  }

  public static int lcm(int... varg) {
    if (varg.length <= 0) return 0;
    if (varg.length == 1) return varg[0];
    return IterUtil.fold_left(Arrays.asList(box(varg)), new Integer(varg[0]),
        new IterUtil.Func<Integer, Integer>() {
          public Integer accept(Integer acc, Integer elt) {
              return lcm(acc, elt);
          }
        });
  }

  // range(1, 13) = [1, 2, 3, ..., 12]
  public static int[] range(int start, int end) {
    if (end < start) return null;

    int size = end - start;
    int[] l = new int[size];
    for (int i = start; i < end; i++) {
      l[i-start] = i;
    }
    return l;
  }

  // fold_left(*, 1, [e1, e2, ... en]) = e1 * e2 * ... * en
  public static int mult(int[] l) {
    return IterUtil.fold_left(Arrays.asList(box(l)), new Integer(1),
        new IterUtil.Func<Integer, Integer>() {
          public Integer accept(Integer acc, Integer elt) {
              return acc * elt;
          }
        });
  }

  // n! = fold_left(*, 1, [1, ... n])
  public static int factorial(int n) {
    return mult(range(1, n+1));
  }

  private static final int[] empty1 = new int[0];
  private static final int[][] empty2 = new int[0][];

  // permutate([1, 2, 3]) = [
  //   [1, 2, 3],
  //   [1, 3, 2],
  //   [2, 1, 3],
  //   [2, 3, 1],
  //   [3, 1, 2],
  //   [3, 2, 1]
  // ]
  // test code:
  //   int[][] pp = permutate(new int[]{1, 2, 3});
  //   for (int i = 0; i < pp.length; i++) {
  //     System.out.println(StringUtil.join(" ", box(pp[i])));
  //   }
  public static int[][] permutate(int[] l) {
    if (l.length == 0) return empty2;
    if (l.length == 1) return new int[][] { l };

    int size = factorial(l.length);
    int[][] res = new int[size][l.length];

    int r = 0;
    // pick one element for the 1st place
    for (int i = 0; i < l.length; i++) {
      // for the remaining elements
      int[] remains = new int[l.length - 1];
      int idx = 0;
      for (int j = 0; j < l.length; j++) {
        if (i != j) remains[idx++] = l[j];
      }
      // generate permutations recursively
      int[][] p_1 = permutate(remains);

      // then, just attach the current element
      for (int j = 0; j < p_1.length; j++) {
        res[r][0] = l[i];
        for (int k = 0; k < p_1[j].length; k++) {
          res[r][k+1] = p_1[j][k];
        }
        r++;
      }
    }

    return res;
  }

  // x's nth bit
  public static boolean bit(int x, int n) {
    return 0 != ((x >>> n) & 0x1);
  }

  // e.g., 3 -> [ 0, 0, ..., 1, 1]
  public static boolean[] bits(int x) {
    boolean[] ans = new boolean[Integer.SIZE];
    for (int i = 0; i < Integer.SIZE; i++) {
      ans[i] = bit(x, i);
    }
    return ans;
  }

}
