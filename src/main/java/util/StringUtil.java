package util;

public class StringUtil {
  public static String capitalize(String s) {
    return Character.toUpperCase(s.charAt(0)) + s.substring(1);
  }

  public static String join(String delim, Object[] list) {
    if (list.length <= 0) return "";

    StringBuilder buf = new StringBuilder();
    buf.append(list[0]);
    for (int i = 1; i < list.length; i++) {
      buf.append(delim);
      buf.append(list[i].toString());
    }
    return buf.toString();
  }
}
