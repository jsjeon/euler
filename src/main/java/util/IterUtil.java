package util;

import java.util.Collection;
import java.util.ArrayList;

public class IterUtil {

  // fold_left : E list -> R -> (R -> E -> R) -> E
  public static <R, E> R fold_left
      (Collection<? extends E> list, R init, Func<R, E> functor) {
    R res = init;
    for (E elt : list) {
      res = functor.accept(res, elt);
    }
    return res;
  }

  // R -> E -> R
  public interface Func<R, E> {
    public R accept(R acc, E elt);
  }

  // filter : E list -> (E -> boolean) -> E list
  public static <E> Collection<E> filter
      (Collection<? extends E> list, Pred<E> pred) {
    Collection<E> res = new ArrayList<E>();
    for (E elt : list) {
      if (pred.apply(elt)) {
        res.add(elt);
      }
    }
    return res;
  }

  // E -> boolean
  public interface Pred<E> {
    public boolean apply(E elt);
  }

}
