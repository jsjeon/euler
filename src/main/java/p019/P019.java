package p019;

/*
 * You are given the following information,
 * but you may prefer to do some research for yourself.
 *
 *   1 Jan 1900 was a Monday.
 *   Thirty days has September, April, June and November.
 *   All the rest have thirty-one, Saving February alone,
 *   Which has twenty-eight, rain or shine.
 *   And on leap years, twenty-nine.
 *   A leap year occurs on any year evenly divisible by 4,
 *   but not on a century unless it is divisible by 400.
 *
 * How many Sundays fell on the first of the month
 * during the twentieth century (1 Jan 1901 to 31 Dec 2000)?
 */

import util.IntUtil;

public class P019 {

  static boolean isLeapYear(int year) {
    if (year % 4 != 0) return false;
    else if (year % 100 != 0) return true;
    else if (year % 400 != 0) return false;
    else return true;
  }

  static String DaytoString(int d) {
    switch (d) {
      case 0: return "Sun";
      case 1: return "Mon";
      case 2: return "Tue";
      case 3: return "Wed";
      case 4: return "Thu";
      case 5: return "Fri";
      case 6: return "Sat";
      default: return "???";
    }
  }

  static int days(int year, int month) {
    switch (month) {
      case 2: return isLeapYear(year) ? 29 : 28;
      case 4:
      case 6:
      case 9:
      case 11: return 30;
      default: return 31;
    }
  }

  static int[] months = IntUtil.range(1, 13);

  // calc next month's first day
  static int nextMonth1stDAY(int year, int month, int first) {
    int days = days(year, month);
    return (first + days) % 7;
  }

  static int count() {
    int firstDay = 1; // Mon 1 Jan 1900
    for (int month : months) { // months in 1900
      firstDay = nextMonth1stDAY(1900, month, firstDay);
    }

    int cnt = 0;
    for (int year = 1901; year <= 2000; year++) {
      for (int month : months) {
        if (firstDay == 0) cnt++;
        System.out.printf("%s %02d/01/%d\n", DaytoString(firstDay), month, year);
        firstDay = nextMonth1stDAY(year, month, firstDay);
      }
    }
    return cnt;
  }

  public static void main(String[] args) {
    int cnt = count();
    System.out.printf("p019: %,d\n", cnt);
  }

}
