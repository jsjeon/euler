package p010;

/* The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
 * Find the sum of all the primes below two million.
 */

import util.IntUtil;
import util.StringUtil;

import java.util.List;
import java.util.ArrayList;

public class P010 {

  static List<Integer> findPrimes(int max) {
    List<Integer> primes = new ArrayList<Integer>();
    for (int i = 2; i < max; i++) {
      if (IntUtil.isPrime(i)) {
        primes.add(i);
      }
    }
    return primes;
  }

  static long sum(List<Integer> l) {
    long acc = 0;
    for (Integer i : l) {
      acc += i;
    }
    return acc;
  }

  public static void main(String[] args) {
    //List<Integer> primes = findPrimes(10);
    List<Integer> primes = findPrimes(2000000);
    long acc = sum(primes);
    System.out.printf("%s = %,d\n",
      StringUtil.join(" + ", primes.toArray()), acc);
  }

}
