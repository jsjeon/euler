package p004;

/* A palindromic number reads the same both ways. The largest palindrome
 * made from the product of two 2-digit numbers is 9009 = 91 × 99.
 * Find the largest palindrome made from the product of two 3-digit numbers.
 */

public class P004 {

  static boolean isPalindromic(String s) {
    int len = s.length();
    for (int i = 0; i < len / 2; i++) {
      if (s.charAt(i) != s.charAt(len - 1 - i)) {
        return false;
      }
    }
    return true;
  }

  static boolean isPalindromic(int n) {
    return isPalindromic(Integer.toString(n));
  }

  static int findLargestPalindrome(int digits) {
    int n = (int)Math.pow(10, digits);
    int min = (int)Math.pow(10, digits-1);
    int max_i = 0, max_j = 0, max = 0;
    for (int i = n; i > min+1; i--) {
      for (int j = i; j > min; j--) {
        int prod = i * j;
        if (isPalindromic(prod) && prod > max) {
          max_i = i; max_j = j; max = prod;
        }
      }
    }
    System.out.printf("largest palindrome of %d-digit product: %,d = %d * %d\n",
        digits, max, max_i, max_j);
    return max;
  }

  public static void main(String[] args) {
    findLargestPalindrome(2);
    int ans = findLargestPalindrome(3);
    System.out.printf("p004: %,d\n", ans);
  }

}
