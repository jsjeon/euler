package p003;

/*
 * The prime factors of 13195 are 5, 7, 13 and 29.
 * What is the largest prime factor of the number 600851475143 ?
 */

import util.IntUtil;

import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

public class P003 {

  public static Set<Integer> getPrimeFactors(long n) {
    Set<Integer> factors = new TreeSet<Integer>();

    double max_p = n; //Math.sqrt(n);
    long rem = n;
    int p = 2;
    while (1 < rem && p <= max_p) {
      if (rem % p == 0) {
        factors.add(p);
        rem = rem / p;
      }
      while(p <= max_p && !IntUtil.isPrime(++p)) ;
    }
    //System.out.printf("%,d's prime factors: %s\n", n, Arrays.toString(factors.toArray()));
    return factors;
  }

  public static void main(String[] args) {
    long n = 13195;
    Set<Integer> factors = getPrimeFactors(n);

    n = 600851475143L;
    factors = getPrimeFactors(n);
    int size = factors.size();
    System.out.printf("p003: %,d\n", factors.toArray()[size-1]);
  }

}
