package p127;

/*
 * The radical of n, rad(n), is the product of distinct prime factors of n.
 * For example, 504 = 2^3 × 3^2 × 7, so rad(504) = 2 × 3 × 7 = 42.
 *
 * We shall define the triplet of positive integers (a, b, c) to be an abc-hit
 * if:
 *   GCD(a, b) = GCD(a, c) = GCD(b, c) = 1
 *   a < b
 *   a + b = c
 *   rad(abc) < c
 *
 * For example, (5, 27, 32) is an abc-hit, because:
 *   GCD(5, 27) = GCD(5, 32) = GCD(27, 32) = 1
 *   5 < 27
 *   5 + 27 = 32
 *   rad(4320) = 30 < 32
 *
 * It turns out that abc-hits are quite rare and
 * there are only thirty-one abc-hits for c < 1000, with ∑c = 12523.
 *
 * Find ∑c for c < 120000.
 */

import util.IntUtil;
import p003.P003;

import java.util.Set;
import java.util.TreeSet;

public class P127 {

  static boolean abc(int a, int b, int c) {
    if (a >= b) return false;
    if (a + b != c) return false;
    if (IntUtil.gcd(a,b) != 1) return false;
    if (IntUtil.gcd(a,c) != 1) return false;
    if (IntUtil.gcd(b,c) != 1) return false;
    //long abc = a * b * c; // may exceed the limit
    Set<Integer> factors_c = P003.getPrimeFactors(c);
    Set<Integer> factors_b = P003.getPrimeFactors(b);
    Set<Integer> factors_a = P003.getPrimeFactors(a);
    Set<Integer> factors = new TreeSet<Integer>();
    factors.addAll(factors_c);
    factors.addAll(factors_b);
    factors.addAll(factors_a);
    int[] fa = IntUtil.unbox(factors.toArray(new Integer[0]));
    int rad = IntUtil.mult(fa);
    return rad < c;
  }

  public static void main(String[] args) {
    int n = 120000;

    long acc = 0;
    for (int c = n-1; c > 2; c--) {
      for (int b = c-1; b > c/2; b--) {
        int a = c - b;
        if (abc(a, b, c)) {
          System.out.printf("(%d, %d, %d)\n", a, b, c);
          acc += c;
        }
      }
    }

    System.out.printf("p127: %,d\n", acc);
  }
}
