Project Euler
=============

A private (or public-to-be) repository to practice popular languages,
such as Java, by solving problems in [Project Euler][euler]

[euler]: https://projecteuler.net

Usage
-----

To build:

    $ gradle build

To run all the problems:

    $ gradle run

To run a single, specific problem:

    $ gradle run -Pargs="p001"

To clean up:

    $ gradle clean

